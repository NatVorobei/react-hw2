import './App.css';
import { Component } from 'react';
import ProductList from './components/cards/product-list';
import Header from './components/header/header';


export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      products: [],
      cartItems: [],
      favourites: []
    }
  }

  componentDidMount() {
    fetch('/products.json')
      .then(response => response.json())
      .then(data => {
          return this.setState({ products: data })
      });

      // get saved cart items from LC
      const cartItems = this.getItemsFromLocalStorage('cartItems');
      if (cartItems instanceof Array) {
        this.setState({cartItems});
      }

      // get saved favourites items from LC
      const favourites = this.getItemsFromLocalStorage('favourites');
      if (favourites instanceof Array) {
        this.setState({favourites});
      }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.cartItems !== this.state.cartItems) {
      this.saveItemsToLocalStorage('cartItems', this.state.cartItems);
    }

    if (prevState.favourites !== this.state.favourites) {
      this.saveItemsToLocalStorage('favourites', this.state.favourites);
    }
  }

  getItemsFromLocalStorage(itemKey) {
    let items = [];
    
    try {
      items = JSON.parse(window.localStorage.getItem(itemKey) || '[]')
    } catch (e) {
      console.log('Inconsistent data in storage');
    }

    return items;
  }

  saveItemsToLocalStorage(itemKey, items) {
    window.localStorage.setItem(itemKey, JSON.stringify(items));
  }

  addProductToCart(productID) {
    // push product ID to cartItems
    let cartItems = [...this.state.cartItems];
    if (cartItems.indexOf(productID) < 0) {
      cartItems.push(productID);
      this.setState({cartItems})
    }
  }

  addProductToFavourites(productID){
    let favourites = [...this.state.favourites];
    if(favourites.indexOf(productID) < 0){
      favourites.push(productID);
      this.setState({favourites})
    }
  } 

  removeProductFromCart(productID) {
    // find item in cartItems and remove
    let cartItems = [...this.state.cartItems];
    this.setState({cartItems: cartItems.filter((item) => item !== productID)})
  }

  removeProductFromFavourites(productID) {
    let favourites = [...this.state.favourites];
    this.setState({favourites: favourites.filter((item) => item !== productID)})
  }

  render(){
    return(
      <>
        <Header cartCount={this.state.cartItems.length} favouriteCount={this.state.favourites.length}/>
        <ProductList 
          products={this.state.products}
          cartItems={this.state.cartItems}
          favourites={this.state.favourites}
          onAddProductToCart={(productID) => this.addProductToCart(productID)}
          onRemoveProductFromCart={(productID) => this.removeProductFromCart(productID)}
          onAddProductToFavs={(productID) => this.addProductToFavourites(productID)}
          onRemoveProductFromFavs={(productID) => this.removeProductFromFavourites(productID)}  
          />
      </>
    )
  }
}


