import { Component } from "react";
import './product-card';
import ProductCard from "./product-card";
import Modal from "../modals";

export default class ProductList extends Component{
    constructor(props) {
        super(props);
        this.state = {
          isModalOpened: false,
          modalData: {},
        };
    }

    openAddToCartModal(productID) {
        this.setState({
            isModalOpened: true, 
            modalData: {action: 'add', productID, text: 'This product will be added to your cart'}
        })
    }    
    
    openRemoveFromCartModal(productID) {
        this.setState({
            isModalOpened: true, 
            modalData: {action: 'remove', productID, text: 'This product will be removed from your cart'}})
    }

    addToFavs(productID){
        this.props.onAddProductToFavs(productID);
    }

    removeFromFavs(productID){
        this.props.onRemoveProductFromFavs(productID);
    }

    closeModal = () => {
        this.setState({isModalOpened: false, modalData: {}})
    }

    submitModal() {
        if (this.state.modalData.action === 'add') {
            this.props.onAddProductToCart(this.state.modalData.productID)
        } else if (this.state.modalData.action === 'remove') {
            this.props.onRemoveProductFromCart(this.state.modalData.productID)
        }
        this.closeModal()
    }
    
    render() {
        const { products } = this.props;
        return (
            <>
                <div className="products">
                    {products.map(product => (
                        <ProductCard
                            key={product.id}
                            name={product.name}
                            price={product.price}
                            image={product.image}
                            sku={product.sku}
                            color={product.color}
                            isAddedToCart={this.props.cartItems.indexOf(product.sku) >= 0}
                            isAddedToFavourites={this.props.favourites.indexOf(product.sku) >= 0}
                            onAddToCart={() => this.openAddToCartModal(product.sku)}
                            onRemoveFromToCart={() => this.openRemoveFromCartModal(product.sku)}
                            onAddToFavourite={() => this.addToFavs(product.sku)}
                            onRemoveFromFavourite={() => this.removeFromFavs(product.sku)}
                        />
                    ))}
                </div>
                <Modal
                    show={this.state.isModalOpened}
                    text={this.state.modalData.text} 
                    onClose={() => this.closeModal()} 
                    onSubmit={() => {this.submitModal()}} 
                    />
            </>
        );
    }
}

