import { Component } from "react";
import Button from "../buttons";
import './product-card.scss';
import { StarNotSolid, StarSolid } from "../icons";
import PropTypes from 'prop-types';

export default class ProductCard extends Component{
    render(){
        const{name, price, image, sku, color,  onAddToCart, onRemoveFromToCart, onAddToFavourite, onRemoveFromFavourite} = this.props; 
        return(
            <>
            <div id={'sku' + sku} className="product">
                <div className="product_image">
                    <img src={image} alt={name} width="300px" height="190px"/>
                </div>
                <div className="product_descr">
                    <p className="product_title">{name}</p>
                    <p>Color: {color}</p>
                    <p>${price}</p>
                </div>
                <div className="product_btns">
                    {this.props.isAddedToCart ? (
                        <Button className="card_btn" text="Remove from cart" onClick={onRemoveFromToCart} />
                    ) : (
                        <Button className="card_btn" text="Add to cart" onClick={onAddToCart} />
                    )}
                    
                    {this.props.isAddedToFavourites ? (
                        <StarSolid
                            width={35}
                            height={35}
                            fill="#007eb9"
                            onClick={onRemoveFromFavourite}
                            />
                    ) : (
                        <StarNotSolid
                            width={35}
                            height={35}
                            fill="#007eb9"
                            onClick={onAddToFavourite}
                            />
                    )}
                </div>
            </div>
            </>
        )
    }
}

ProductCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    image: PropTypes.string,
    sku: PropTypes.number,
    color: PropTypes.string,
    onAddToCart: PropTypes.func,
    onRemoveFromToCart: PropTypes.func,
    onAddToFavourite: PropTypes.func,
    onRemoveFromFavourite: PropTypes.func
};