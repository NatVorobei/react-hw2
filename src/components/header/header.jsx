import { Component } from "react";
import './header.scss';
import PropTypes from 'prop-types';

export default class Header extends Component{
    

    render(){
        const{cartCount, favouriteCount} = this.props;
        return(
            <>
                <div className="header_container">
                    <div className="header_logo">
                        <img src="/logo.png" alt="logo" width="400px"/>
                    </div>
                    <div className="header_actions">
                        <div className="products_cart">
                            <img src="/shopping-cart.png" alt="cart" width="30px"/>
                            <span>{cartCount}</span>
                        </div>
                        <div className="products_favourite">
                            <img src="/star.png" alt="star" width="30px"/>
                            <span>{favouriteCount}</span>
                        </div>
                    </div>
                    
                </div>
            </>
        )
    }
}

Header.propTypes = {
    cartCount: PropTypes.number,
    favouriteCount: PropTypes.number,
};
