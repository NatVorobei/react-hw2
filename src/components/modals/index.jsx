import { Component } from "react";
import Button from "../buttons";
import '../modals/modal.scss';
import PropTypes from 'prop-types';

export default class Modal extends Component {
    
    render(){
        if (!this.props.show){
            return null;
        }

        return (
            <>
                <div className="overlay" onClick={this.props.onClose}></div>
                <div className="modal">
                    <div className="modal_text">
                        <p>{this.props.text}</p>
                    </div>

                    <div className="modal_btns">
                        <Button className={"btn modal_btn"} text='Submit' onClick = {this.props.onSubmit}/>
                        <Button className={"btn modal_btn"} text='Cancel' onClick = {this.props.onClose}/>
                    </div>
                    
                </div>
            </>
        )
    }
}

Modal.propTypes = {
    show: PropTypes.bool,
    text: PropTypes.string,
    onClose: PropTypes.func,
    onSubmit: PropTypes.func,
};
